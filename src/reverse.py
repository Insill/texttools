#!/usr/bin/env python3
# encoding -*- utf-8 -*-
'''
Created on 7.2.2017

@author: Insill
'''

import sys


def main():
    string = sys.argv[0]
    new = check(string)
    palindrome(new)


def check(string):
    if(isinstance(string, str)):  # in Python 3 all strings are Unicode objects by default
        print("Unicode")
        return string
    elif(not(isinstance(string, str))):  # if the string isn't an Unicode object, it gets converted
        print("Not Unicode")
        new = string.decode("utf-8")
        return new


def palindrome(string):
    status = False
    palindrome = string[::-1]
    if(palindrome == string):
        status = True
        return status
    else:
        return status

main()
