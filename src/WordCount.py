#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, re
from operator import itemgetter


def main():
	vowelcount = {"a": 0, "e":0, "i":0, "o":0, "y":0, "ä":0, "ö":0}
	consonantcount = {"b":0, "c":0, "d":0, "f":0, "g":0, "h":0, "j":0,
					"k":0, "l":0, "m":0, "n":0, "p":0, "q":0, "r":0,
					"s":0, "t":0, "v":0, "x":0, "z":0}	
	text = get_text()
	wordcount = count_words(text)
	vowelcount = count_vowels(text, vowelcount)
	consonantcount = count_consonants(text, consonantcount)
	common_words(wordcount)

def get_text():
	text = ""
	path = ""
	punct = '!"”“„…€#$%&()*+,./:;<=>?@[\]^_`´{|}~¶¤ï»¿¦ðÿ˜¬œ§©'
	while (not os.path.exists(path)):
		try:
				path = input("\n===========\nPlease give the text file's full path:\t")
				file = open(path, "r", errors="ignore", encoding="utf-8")
				text = file.read().lower()
				text = re.sub(r'-\n(\w+ *)', r'\1\n', text)
				text = text.translate(str.maketrans('', '', punct))
				file.close()
		except:
			print("\n===========\nThere is no file or the path is not valid \n===========\n")	
	return text


def count_words(text):
	wordcount = {}
	stopwords = get_stopwords()
	for word in text.split():
		if word not in wordcount and word not in stopwords:
			if re.match("[^0-9,$]+$", word):
				wordcount[word] = 1
		else:
			wordcount[word] += 1
	print("\n--------\nTotal words: " + str(len(wordcount)) + "\n-------")
	print("--------\nWord count\n-------\n")
	
	for k, v in sorted(wordcount.items()):
		print("{0} {1}".format(k, v))
	return wordcount


def count_vowels(text, vowelcount):
	print("\n--------- Vowels -----------\n")
	for c in text: 
		if c in vowelcount: 
			vowelcount[c] += 1
	for k, v in sorted(vowelcount.items()):
		print("{0} {1}".format(k, v))
	return vowelcount


def count_consonants(text, consonantcount):
	print("\n--------- Consonants -------\n")
	for c in text:
		if c in consonantcount:
			consonantcount[c] += 1
	for k, v in sorted(consonantcount.items()):
		print("{0} {1}".format(k, v))		
	return consonantcount


def common_words(wordcount):
	common = dict(sorted(wordcount.items(), key=itemgetter(1), reverse=True)[:10])
	print("\n-----\n10 most common words\n------\n" + str(common))

def get_stopwords():
	stopwords = []
	path = "/stopwords/"
	pass

main()
